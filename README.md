Requirements
============

Redis server locally [installed](https://redis.io/topics/quickstart)  
Node v10.16.3  
Typescript globally available _"npm i -g typescript@3.7.2"_  

Startup
=======

On project working directory:  
In one terminal window: **`tsc -w`**  
In another one: **`npm start`**  

Available routes
================  
BASE: http://localhost:3000  

**GET**    /loan/`:id`  
**POST**   /loan  
