import { Server } from './server'
import { loanRouter } from './routes/loan.router'
import Queue from './config/queue'

Queue.process()

const server = new Server()
server.bootstrap([loanRouter])
  .then(() => console.log('Server up and running'))
  .catch(error => {
    console.log(error)
    process.exit(1)
  })
