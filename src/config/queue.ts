import * as bull from 'bull';

import * as jobs from '../jobs';

const queues = Object.values(jobs).map(job => {
  return ({
    bull: new bull(job.key, 'redis://127.0.0.1:6379'),
    name: job.key,
    handle: job.handle
  })
})

export default {
  queues,
  add(name, data) {
    const queue = this.queues.find(queue => queue.name === name);
    return queue.bull.add(data, queue.options);
  },
  process() {
    return this.queues.forEach(queue => {
      queue.bull.process(queue.handle);
      queue.bull.on('failed', (job, err) => {
        console.log('Job failed', queue.key, job.data);
        console.log(err);
      });
    })
  }
};

