import * as mongoose from 'mongoose'
import { v4 as uuid } from 'uuid'

import { Status } from '../enums/status'
import { Result } from '../enums/result'
import { RefusedPolicy } from '../enums/refusedPolicy'

export interface ILoan extends mongoose.Document {
  name: string,
  cpf: string,
  birthdate: Date,
  amount: number,
  terms: number,
  income: number,
  result?: Result,
  status: Status,
  approvedAmount?: number,
  approvedTerms?: number,
  refusedPolicy?: string
}

const loanSchema = new mongoose.Schema({
  id: {
    type: String
  },
  name: {
    type: String,
    required: true
  },
  cpf: {
    type: String,
    required: true
  },
  birthdate: {
    type: Date,
    required: true
  },
  amount: {
    type: Number,
    required: true
  },
  approvedAmount: {
    type: Number,
    default: null
  },
  terms: {
    type: Number,
    enum: [6, 9, 12],
    required: true
  },
  approvedTerms: {
    type: Number,
    default: null
  },
  income: {
    type: Number,
    required: true
  },
  status: {
    type: String,
    enum: [Status.PROCESSING, Status.COMPLETED],
  },
  result: {
    type: String,
    enum: [Result.APPROVED, Result.REFUSED, null],
    default: null
  },
  refusedPolicy: {
    type: String,
    enum: [RefusedPolicy.AGE, RefusedPolicy.SCORE, RefusedPolicy.COMMITMENT, null],
    default: null
  }
}, { versionKey: false })

function saveMiddleware(next) {
  const loan: ILoan = this
  if (!loan.id) {
    loan.id = uuid()
  }

  if (!loan.status) {
    loan.status = Status.PROCESSING
  }
  next()
}

loanSchema.pre('save', saveMiddleware)

export const Loan = mongoose.model<ILoan>('Loan', loanSchema)
