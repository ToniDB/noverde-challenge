import { Application, Request, Response } from 'express'
import { body, validationResult } from 'express-validator'

import { Router } from '../entities/router'
import { Loan } from '../models/loan'
import Queue from '../config/queue'
import { Jobs } from '../constants'

class LoanRouter extends Router {

  applyRoutes(application: Application): any {
    application.post(
      '/loan',
      [
        body('name').exists().notEmpty({ ignore_whitespace: true }),
        body('cpf').exists().notEmpty({ ignore_whitespace: true }),
        body('birthdate').toDate(),
        body('amount').isFloat({ min: 1000, max: 4000 }).toFloat(),
        body('terms').isNumeric({ no_symbols: true }).isIn([6, 9, 12]),
        body('income').isNumeric({ no_symbols: true })
      ],
      this.save
    )
    application.get('/loan/:id', this.find)
  }

  private async save(req: Request, res: Response) {
    try {
      const errors = validationResult(req)
      if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() })
      }
      const doc = await Loan.create(req.body)

      Queue.add(Jobs.PROCESS_LOAN, { id: doc.id })
      
      return res.send({ id: doc.id })
    } catch (error) {
      res.status(500).send({ error })
    }
  }

  private async find(req: Request, res: Response) {
    try {
      const id = req.params.id;
      const doc = await Loan.findOne({ id })
      
      if (!doc) {
        return res.status(404).send({ error: 'Loan request not found' })
      }
      res.send({
        status: doc.status,
        result: doc.result,
        refused_policy: doc.refusedPolicy,
        amount: doc.approvedAmount,
        terms: doc.approvedTerms
      })
    } catch (error) {
      res.status(500).send({ error })
    }
  }
}

export const loanRouter = new LoanRouter()
