export enum Result {
  APPROVED = 'approved',
  REFUSED = 'refused'
}
