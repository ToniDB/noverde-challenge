export enum RefusedPolicy {
  AGE = 'age',
  SCORE = 'score',
  COMMITMENT = 'commitment'
}
