import axios from '../utils/axios'
import { Job } from 'bull'

import { Loan, ILoan } from '../models/loan'
import { Jobs } from '../constants'
import { ScoreResult } from '../interfaces/scoreResult'
import { CommitmentResult } from '../interfaces/commitmentResult'
import { Status } from '../enums/status'
import { Result } from '../enums/result'
import { RefusedPolicy } from '../enums/refusedPolicy'

function calculateAge(birthday: Date) {
  const ageDifMs = Date.now() - birthday.getTime();
  const ageDate = new Date(ageDifMs);
  return Math.abs(ageDate.getUTCFullYear() - 1970);
}

async function applyAgePolicy(loan: ILoan): Promise<void> {
  if (loan.result === Result.REFUSED) return

  const age = calculateAge(loan.birthdate)
  if (age < 18) {
    loan.status = Status.COMPLETED
    loan.refusedPolicy = RefusedPolicy.AGE
    loan.result = Result.REFUSED
    await loan.save()
  }
}

async function applyScorePolicy(loan: ILoan): Promise<void>  {
  if (loan.result === Result.REFUSED) return
  const scoreResponse = await axios.post<ScoreResult>('/score', { cpf: loan.cpf })
  if (scoreResponse.data.score < 600) {
    loan.status = Status.COMPLETED
    loan.refusedPolicy = RefusedPolicy.SCORE
    loan.result = Result.REFUSED
    await loan.save()
  }
}

export default {
  key: Jobs.PROCESS_LOAN,
  options: {
    delay: 5000,
  },
  handle: async (job: Job) => {
    try {
      const { id } = job.data;

      const loan = await Loan.findOne({ id })
      console.log('document', loan)
      await applyAgePolicy(loan)
      await applyScorePolicy(loan)

    } catch (error) {
      console.log(error)
    }
  }
}
