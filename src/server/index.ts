import * as express from 'express'
import * as mongoose from 'mongoose'
import { json } from 'body-parser'

import { enviroment } from '../utils/enviroment'
import { Router } from '../entities/router'

export class Server {

  application: express.Application

  public bootstrap(routes: Router[] = []) {
    return this.initializeDatabase()
      .then(() => this.initRoutes(routes).then(() => this))
  }

  private initializeDatabase() {
    (mongoose as any).Promise = global.Promise
    console.log(enviroment.db.url)
    return mongoose.connect(enviroment.db.url, { useNewUrlParser: true, useUnifiedTopology: true })
  }

  private initRoutes(routes: Router[]): Promise<express.Application> {
    return new Promise((resolve, reject) => {
      try {
        this.application = express()
        this.application.use(json())
        routes.forEach(route => route.applyRoutes(this.application))
        this.application.listen(enviroment.server.port, () => { resolve(this.application) })
      } catch (error) {
        reject(error)
      }
    })
  }

}
