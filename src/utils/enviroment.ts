import { config } from 'dotenv'

const env = config({}).parsed

export const enviroment = {
  server: {
    port: env.SERVER_PORT || 3000
  },
  db: {
    url: env.DB_URL || `mongodb://localhost:27017/loan-processor`
  }
}
