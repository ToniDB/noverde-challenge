import axios, { AxiosRequestConfig } from 'axios'

const axiosInstance = axios.create({
  baseURL: 'https://challenge.noverde.name'
})


axiosInstance.interceptors.request.use((config: AxiosRequestConfig) => {
  config.headers = { 'x-api-key': `FNzBjSMvWI1p3Z7arIHHE2VRnknBiKU88afCzZZj` };
  return config;
});

export default axiosInstance;
